#
# ~/.bashrc
#

# Import bash git prompt
GIT_PROMPT_ONLY_IN_REPO=1
source ~/.bash-git-prompt/gitprompt.sh

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\W]\$ '
# >>> Added by cnchi installer
BROWSER=/usr/bin/chromium
EDITOR=nvim
PATH=/home/gilgames/.npm-global/bin:/home/gilgames/.npm-global/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/opt/flutter/bin:/home/gilgames/.local/lib/python3.7/site-packages:/opt/flutter/bin/cache/dart-sdk/bin:/home/gilgames/.emacs.d/bin

source ~/.bashrc.aliases
