"    _____ _ _                                 _       _
"   / ____(_) |                               ( )     (_)
"  | |  __ _| | __ _  __ _ _ __ ___   ___  ___|/__   ___ _ __ ___  _ __ ___
"  | | |_ | | |/ _` |/ _` | '_ ` _ \ / _ \/ __| \ \ / / | '_ ` _ \| '__/ __|
"  | |__| | | | (_| | (_| | | | | | |  __/\__ \  \ V /| | | | | | | | | (__
"   \_____|_|_|\__, |\__,_|_| |_| |_|\___||___/   \_/ |_|_| |_| |_|_|  \___|
"               __/ |
"              |___/

set nocompatible " be iMproved
filetype off " required!
filetype plugin indent on
syntax on

" vim-plug
" ------------------------------------------------------------------------------
" https://github.com/junegunn/vim-plug

" :PlugUpdate - install or update plugins
" :PlugClean! - Remove unused directories (bang version will clean without prompt)

call plug#begin('~/.config/nvim/plugged')

" CoVim
Plug 'FredKSchott/CoVim'

" Motion
Plug 'goldfeld/vim-seek'

" Autocompletion
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" Plug 'ervandew/supertab'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" C/C++
Plug 'zchee/deoplete-clang'

" Java
Plug 'artur-shaik/vim-javacomplete2'

" HTML/CSS
Plug 'jvanja/vim-bootstrap4-snippets'

" Javascript
Plug 'ternjs/tern_for_vim', { 'for': ['javascript', 'javascript.jsx'] }
Plug 'carlitux/deoplete-ternjs', { 'for': ['javascript', 'javascript.jsx'] }
Plug 'othree/jspc.vim', { 'for': ['javascript', 'javascript.jsx'] }

" Python
Plug 'zchee/deoplete-jedi'

" Haskell
" Plug 'eagletmt/neco-ghc'
Plug 'mkasa/neco-ghc-lushtags'

" Ctags
Plug 'majutsushi/tagbar'

" Color schemes
Plug 'altercation/vim-colors-solarized'
Plug 'romainl/flattened'

" Statusline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Text objects
Plug 'kana/vim-textobj-user' | Plug 'whatyouhide/vim-textobj-xmlattr'

" Plug 'Lokaltog/vim-easymotion'
" Plug 'Xuyuanp/nerdtree-git-plugin'
" Plug 'airblade/vim-gitgutter'
Plug 'ctrlpvim/ctrlp.vim'
" Plug 'junegunn/vim-easy-align'
" Plug 'lfilho/cosco.vim' " Comma and semi-colon insertion bliss for vim.
" Plug 'rking/ag.vim'
" Plug 'scrooloose/nerdtree'
Plug 'tomtom/tcomment_vim'
" Plug 'tpope/vim-fugitive'
" Plug 'tpope/vim-surround'
" Plug 'zhaocai/GoldenView.Vim'
" Plug 'neomake/neomake'
" Plug 'airblade/vim-rooter'
" Plug 'Shougo/denite.nvim'

call plug#end()

" Preferences
set autoindent " Match indents on new lines.
set smartindent " Intellegently dedent / indent new lines based on rules.
set cursorline                         " highlighted cursor row
set expandtab                          " insert spaces instead when pressing <tab>
set smarttab " let's tab key insert 'tab stops', and bksp deletes tabs.
set formatoptions-=cro                 " no annoying comment autoformat foo
set noerrorbells                       " don't beep, asshole
set visualbell                         " don't beep
set number
set relativenumber
set shiftwidth=4                       " default to 4 spaces for indentation
set tabstop=4                          " use four space chars when pressing <tab>
set wildmenu

" Search
set ignorecase " case insensitive search
set smartcase " If there are uppercase letters, become case-sensitive.
set incsearch " live incremental searching
set showmatch " live match highlighting
set hlsearch " highlight matches

" Appearence
syntax on
" let g:solarized_termcolors=256
" set t_Co=256
set background=dark
colorscheme solarized
" let g:solarized_termtrans = 1 " This gets rid of the grey background

" Encoding
set encoding=utf-8

" Mapleader
let mapleader=','

" Better movement between splits
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" We have VCS -- we don't need this stuff.
set nobackup " We have vcs, we don't need backups.
set nowritebackup " We have vcs, we don't need backups.
set noswapfile " They're just annoying. Who likes them?

" allow the cursor to go anywhere in visual block mode.
set virtualedit+=block

" Backups
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup

" Spell checking and automatic wrapping at 72 characters to commit messages
autocmd Filetype gitcommit setlocal spell textwidth=72

" Bacause make doesn't like spaces...
autocmd FileType make setlocal noexpandtab

" Local .vimrc name
let g:localrc_filename = ".local.vimrc.vim"

" C includes directories for syntastic
let g:syntastic_c_config_file = '.includes.vim'

" Deoplete settings
let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_ignore_case = 1
let g:deoplete#enable_smart_case = 1
let g:deoplete#enable_refresh_always = 0
let g:deoplete#omni#functions = {}
let g:deoplete#sources = {}
" let g:deoplete#sources._ = []
" let g:deoplete#file#enable_buffer_path = 1
" let g:deoplete#omni#input_patterns = get(g:,'deoplete#omni#input_patterns',{})
" let g:deoplete#omni#input_patterns.java = [
"             \'[^. \t0-9]\.\w*',
"             \'[^. \t0-9]\->\w*',
"             \'[^. \t0-9]\::\w*',
"             \]
let g:deoplete#ignore_sources = {}
" let g:deoplete#ignore_sources._ = ['javacomplete2']
inoremap <expr><C-h> deoplete#mappings#smart_close_popup()."\<C-h>"
inoremap <expr><BS> deoplete#mappings#smart_close_popup()."\<C-h>"

" let g:deoplete#ignore_sources.java = ['omni']
call deoplete#custom#set('javacomplete2', 'mark', '[jc]')
" let g:deoplete#ignore_sources.java = ['javacomplete2']
" call deoplete#custom#set('omni', 'mark', '')

let g:deoplete#omni_patterns = {}
" let g:deoplete#omni_patterns.jsp = ['[^. \t0-9]\.\w*']
" let g:deoplete#omni_patterns.html = '<[^>]*'
" let g:deoplete#omni_patterns.xml  = '<[^>]*'
" let g:deoplete#omni_patterns.md   = '<[^>]*'
" let g:deoplete#omni_patterns.css   = '^\s\+\w\+\|\w\+[):;]\?\s\+\w*\|[@!]'
" let g:deoplete#omni_patterns.scss   = '^\s\+\w\+\|\w\+[):;]\?\s\+\w*\|[@!]'
" let g:deoplete#omni_patterns.sass   = '^\s\+\w\+\|\w\+[):;]\?\s\+\w*\|[@!]'
" let g:deoplete#omni_patterns.javascript = '[^. \t]\.\%(\h\w*\)\?'
" let g:deoplete#omni_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)\w*'
" let g:deoplete#omni_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\w*\|\h\w*::\w*'
" let g:deoplete#omni_patterns.go = '[^.[:digit:] *\t]\.\w*'
" let g:deoplete#omni_patterns.ruby = ['[^. *\t]\.\w*', '\h\w*::']
" let g:deoplete#omni_patterns.php = '\h\w*\|[^. \t]->\%(\h\w*\)\?\|\h\w*::\%(\h\w*\)\?'
" let g:deoplete#omni_patterns.python = ['[^. *\t]\.\h\w*\','\h\w*::']
" let g:deoplete#omni_patterns.python3 = ['[^. *\t]\.\h\w*\','\h\w*::']
let g:deoplete#omni_patterns.java = '[^. *\t]\.\w*'

" Javacomplete
" autocmd FileType java setlocal omnifunc=javacomplete#Complete

" " Python
let g:deoplete#sources#jedi#show_docstring = 0

let g:deoplete#sources = {}
let g:deoplete#sources._ = ['buffer']
let g:deoplete#sources.coffee = ['buffer', 'tag', 'member', 'file', 'omni']
imap     <Nul> <C-Space>
inoremap <expr><C-Space> deoplete#mappings#manual_complete()
inoremap <expr><BS>      deoplete#mappings#smart_close_popup()."\<C-h>"

" Haskell
let g:haskellmode_completion_ghc = 0
autocmd FileType haskell setlocal omnifunc=necoghc#omnifunc

" Tern Autocompletion
" close the preview window when you're not using it
" let g:SuperTabClosePreviewOnPopupClose = 1
" or just disable the preview entirely
set completeopt-=preview

" C
let g:deoplete#sources#clang#libclang_path="/home/gilgames/libclang.so"
let g:deoplete#sources#clang#clang_header="/usr/lib/llvm-3.8/lib/clang"

" Let <Tab> also do completion
inoremap <silent><expr><Tab>
\ pumvisible() ? "\<C-n>" :
\ deoplete#mappings#manual_complete()

inoremap <silent><expr><s-Tab>
\ pumvisible() ? "\<C-p>" :
\ deoplete#mappings#manual_complete()

" pressing Enter will complete&close popup if visible (so next Enter works); else: break undo
inoremap <silent><expr> <Cr> pumvisible() ?
            \ deoplete#mappings#close_popup() : "<C-g>u<Cr>"

" Escape will close autocompletion and go to Normal mode
" inoremap <silent><expr> <Esc> pumvisible() ? "<C-e><Esc>" : "<Esc>"

" Close the documentation window when completion is done
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" Ctags executable name
let g:tagbar_ctags_bin = 'unictags'

" Toggle Tagbar
nmap <F8> :TagbarToggle<CR>

" map jk to <esc>
imap jk <esc>
tmap jk <c-\><c-n>

" ditch the shift key
nmap ; :
vmap ; :

" " Neomake
" autocmd! BufWritePost,BufEnter * Neomake

" " Neoformat
" augroup astyle
"   autocmd!
"   autocmd BufWritePre * Neoformat
" augroup END

" Denite
" nnoremap <C-p> :<C-u>Denite file_rec<CR>
" nnoremap <leader>s :<C-u>Denite buffer<CR>
" nnoremap <leader><Space>s :<C-u>DeniteBufferDir buffer<CR>
" nnoremap <leader>8 :<C-u>DeniteCursorWord grep:. -mode=normal<CR>
" nnoremap <leader>/ :<C-u>Denite grep:. -mode=normal<CR>
" nnoremap <leader><Space>/ :<C-u>DeniteBufferDir grep:. -mode=normal<CR>
" nnoremap <leader>d :<C-u>DeniteBufferDir file_rec<CR>

" CtrlP settings
let g:ctrlp_map = '<c-p>'
let g:ctrlp_max_height = 30

" Make CtrlP ignore files read from .gitignore
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']

" Airline
let g:airline_powerline_fonts = 1
let g:airline_theme = "solarized"
let g:airline_solarized_bg='dark'

" Shortcuts
"
" ,"        put double quotes around current word
" ,'        put single quotes around current word
" ,s        save
" ,W        delete trailing spaces
" ,ft       fold tag
" ,v        select text that was just pasted
" ,ev       open .vimrc in a vsplit window
" ,a        disable search highlightning then redraw the screen
"
nnoremap <Leader>s :w<CR>
nnoremap <Leader>W : %s:\s\+$::<CR>: let @/=''<CR>
nnoremap <leader>v V`]
nnoremap <leader>ev <C-w><C-v><C-l>:e ~/.config/nvim/init.vim<CR>
nnoremap <leader>ft Vatzf
nnoremap <Leader>" viw<esc>a"<esc>hbi"<esc>lel
nnoremap <Leader>' viw<esc>a'<esc>hbi'<esc>lel
nnoremap <Leader>a :nohlsearch<CR>:redraw!<CR>
nnoremap <Leader>vs <C-w>v<C-w>l
nnoremap <Leader>hs <C-w>s<C-w>j
